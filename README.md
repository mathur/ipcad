# Introduction
This project bridges a GUI (FreeCAD) and programmatic (CadQuery) interface.
Users can automatically synthesize CadQuery sub-programs / queries based
on GUI-based selections.
This enables the ease of use of GUI, 
coupled with the robustness and re-usability of programming.

To try the project in an already set-up virtual machine with instructions, please download the virtual machine at : https://owncloud.mpi-sws.org/index.php/s/fAT62pwBpgkiRdM

The original repository of this project is at: https://gitlab.mpi-sws.org/mathur/prodmcad

# Requirements
[FreeCAD](https://www.freecadweb.org/)

[CadQuery (version compatible with FreeCAD)](https://github.com/dcowden/cadquery)
Make sure that the cadquery repo is updated to the latest commit.

The project uses pyperclip to copy queries to the clipboard.
To install pyperclip, you will need to to start the FreeCAD Python interpreter from the terminal / command line.
```
import pip
pip.main(['install'] + ['pyperclip'])
```

# Folder structure and repository contents
The `prodm` folder consists relevant background scripts for this project.
The `Experiment` folder consists of examples and experiments presented in the paper.


# Set-up
Open FreeCAD.
We use FreeCAD's Python console.
Add project to the FreeCAD Python path and run the requisite script.
```
import sys
sys.path.append("path_to_cadquery")
sys.path.append("path_to_repo")
import Example
```

To reload the script after making changes, you can type
```
reload(Example)
```
if the changes were focussed on the Example script. 
Else, you need to re-run FreeCAD.

# Usage
In the GUI, elements can be selected using the standard interaction procedure.
Once the requisite elements are selected, pressing 'M' on the keyboard synthesizes the relevant query. This query is also copied onto the clipboard.
