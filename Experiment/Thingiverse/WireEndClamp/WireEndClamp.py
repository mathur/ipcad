# Inspired by the model at: thingiverse.com/thing:3870946


import cadquery as cq
import Part
import random
import json


# Number of iterations for the model
num_iter = 50

print("WireEndClamp")

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    """
       Wire End Clamp for Model Railroad.
       Clamp Bus wire to end of layout, with slots to separate
       and hold two wires in position.
    """
    #  PARAMETERS - values in brackets are suggested starting values)  */
    # [Open Parameters:] */
    # Width of clamp
    b_width = random.uniform(4, 100)  # Default: 65
    vars["b_width"] = str(b_width)

    # Height of clamp
    b_height = random.uniform(1, 100)  # Default: 30
    vars["b_height"] = str(b_height)

    # Width of gap
    gap_w = random.uniform(4, 100)  # Default: 30
    vars["gap_w"] = str(gap_w)

    # Depth of gap
    gap_d  = random.uniform(1, 5)  # Default: 2
    vars["gap_d"] = str(gap_d)

    # Distance between gaps
    gap_sep  = random.uniform(1, 100)  # Default: 20
    vars["gap_sep"] = str(gap_sep)

    # Create the main bracket
    main_body = cq.Workplane("XY").box(b_width, b_height, 5, centered=(False, False, False))

    # calculate and build the gaps offset from edge of bracket
    offset = (b_width/2.0) -(gap_sep/2.0)-(gap_w/2.0)
    offsetted_workplane = main_body.faces("<Z").transformed(offset=(offset, 0, 0)) # The base face
    gap_1 = offsetted_workplane.box(gap_w, b_height, gap_d, centered=(False, False, False), combine=False)
    gap_2 = offsetted_workplane.transformed(offset=(gap_sep, 0, 0)).box(gap_w, b_height, gap_d, centered=(False, False, False), combine=False)
    # Union the gaps
    gap = gap_1.union(gap_2)

    # Countersink screw holes
    offsetted_workplane = main_body.faces("<Z")
    # csk hole 1
    csk_1_a = offsetted_workplane.transformed(offset=(10.0,b_height/2.0,0)).circle(radius=2.0).extrude(5, combine=False)
    csk_1_b = offsetted_workplane.transformed(offset=(10.0,b_height/2.0,3)).circle(radius=2.0).workplane(offset=2).circle(radius=3.675).loft(combine=False)
    csk_1 = csk_1_a.union(csk_1_b)

    # csk hole 2
    offset = b_width - 10
    csk_2_a = offsetted_workplane.transformed(offset=(offset,b_height/2.0,0)).circle(radius=2.0).extrude(5, combine=False)
    csk_2_b = offsetted_workplane.transformed(offset=(offset,b_height/2.0,3)).circle(radius=2.0).workplane(offset=2).circle(radius=3.675).loft(combine=False)
    csk_2 = csk_2_a.union(csk_2_b)

    csk = csk_1.union(csk_2)

    # Cuts
    cuts = csk.union(gap)
    # Cut the gaps from the main bracket
    result = main_body.cut(cuts)

    # Export to FreeCAD
    # Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
