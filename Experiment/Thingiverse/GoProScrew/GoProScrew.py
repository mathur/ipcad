# Design inspired by https://www.thingiverse.com/thing:1643650

import cadquery as cq
import Part

import random
import json

# Number of iterations for the model
num_iter = 50

print("GoProScrew")

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    # Params
    w_grip = random.uniform(1, 100)  # Default: 20.0
    vars["w_grip"] = str(w_grip)

    h_bottom = random.uniform(1, w_grip)  # Default: 10.0
    vars["h_bottom"] = str(h_bottom)

    sphere_ratio = random.uniform(1, 1.5)  # Default: 1.25
    vars["sphere_ratio"] = str(sphere_ratio)

    d_max = random.uniform(1, 100)  # Default: 9.2
    vars["d_max"] = str(d_max)

    d_min = random.uniform(1, d_max)  # Default: 8.7
    vars["d_min"] = str(d_min)

    d_outer = random.uniform(1,100)  # Default: 14.0
    vars["d_outer"] = str(d_outer)

    d_inner = random.uniform(1,d_outer)  # Default: 5.5
    vars["d_inner"] = str(d_inner)

    d_grip = random.uniform(1, w_grip)  # Default: 11.0
    vars["d_grip"] = str(d_grip)

    h_top = random.uniform(1, 100)  # Default: 5.0
    vars["h_top"] = str(h_top)

    h_mid = random.uniform(1, 100)  # Default: 35.0
    vars["h_mid"] = str(h_mid)

    grip_ratio = random.uniform(1,2)  # Default: 1.75
    vars["grip_ratio"] = str(grip_ratio)

    grip_angle = random.uniform(0,60)  # Default: 20.0
    vars["grip_angle"] = str(grip_angle)

    print("Iteration", iter, vars)
    # base
    cube = cq.Workplane("XY").box(w_grip, w_grip, h_bottom, centered=(True, True, False))
    sphere = cq.Workplane("XY").sphere(w_grip * sphere_ratio /2.0)
    base = cube.intersect(sphere)
    cyl_cut = cq.Workplane("XY")
    for angle in range(0, 271, 90):
        temp = cq.Workplane("XY").circle(h_bottom / 2.0).extrude(h_bottom + (w_grip / 2.0)).rotate(axisStartPoint=(0,0,0), axisEndPoint=(1,0,0), angleDegrees=grip_angle).translate(vec=(0, -w_grip/grip_ratio, -w_grip/4.0)).rotate(axisStartPoint=(0,0,0),axisEndPoint=(0,0,1), angleDegrees=angle)
        cyl_cut = cyl_cut.union(temp)
    base = base.cut(cyl_cut)

    # Base cut
    base_cut = cq.Workplane("XY").workplane(offset=-1).circle(d_grip/2.0).extrude(h_bottom+1, combine=False)

    # Base hex cut from above
    base_cut_hex = base.faces(">Z").workplane(offset=-1).polygon(nSides=6, diameter=d_max).workplane(offset=(h_mid+1)).polygon(nSides=6, diameter=d_min).loft(combine=False)

    # Rising cyl
    base = base.faces(">Z").circle(d_outer/2.0).extrude(h_mid+h_top)

    # Cut inner diameter from the result
    result = base.faces(">Z").hole(diameter=d_inner, depth= h_top)

    # Make cuts
    result = result.cut(base_cut)
    result = result.cut(base_cut_hex)

    # Export to FreeCAD
    # Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars

base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
