$fn = 360;

d_inner = 98;
d_outer = 126;

d_screws = 108.1;
d_screw = 4;
d_screwcap = 6;
screw_sink = 2;
screw_count = 4;

height = 6;

fin_thickness = 2.5;
fin_sink = 1.5;
fin_count = 12;
fin_angle = 30;

r_inner = d_inner / 2;
fin_spacing = d_inner / (fin_count + 1); // center-to-center


// the whole thing
union()
{
    // ring with screw holes
    difference()
    {
        // ring
        color ("dodgerblue")
        difference()
        {
            cylinder (d1 = d_outer, d2 = (d_outer + d_inner) / 2, h = height, center = true);
            cylinder (d = d_inner, h = 100, center = true);
        }

        // screw holes
        for (i = [0 : 360 / screw_count : 360 - 360 / screw_count])
        {
            color ("crimson")
            rotate (i)
            translate ([d_screws / 2, 0, 0])
            union()
            {
                // screwcap hole
                cylinder (d = d_screw, h = 100, center = true);
                translate([0, 0, 50 + height / 2 - screw_sink])

                // screwhole
                cylinder (d = d_screwcap, h = 100, center = true);
            }
        }
    }

    // fins
    color ("peru")
    intersection()
    {
        /// fins... rignt?
        f(i = [0 : fin_count - 1])
        {
            translate ([-r_inner + fin_spacing + fin_spacing * i, 0, 0])
            rotate ([0, fin_angle, 0])
            cube ([fin_thickness, d_screws - d_screwcap, 100], center = true);or
        }

        translate ([0, 0, -fin_sink / 2])
        cylinder (d = d_screws - d_screwcap, h = height - fin_sink, center = true);
    }
}