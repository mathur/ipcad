# Inspired by https://www.thingiverse.com/thing:3834348

import cadquery as cq
import Part
import random
import json

print("SpeakerGrill")
# Number of iterations
num_iter = 50
# Prepare the json file
json_fname = "Vars_d.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(40, 50):
    print("In iteration", iter)
    vars = dict()
    d_inner = random.uniform(50, 150)  # Default: 98
    vars["d_inner"] = str(d_inner)
    r_inner = d_inner / 2
    d_outer = random.uniform(d_inner, 150)  # Default: 126
    vars["d_outer"] = str(d_outer)
    r_outer = d_outer / 2

    height = random.uniform(1, 100)  # Default: 6
    vars["height"] = str(height)

    d_screws = random.uniform(d_inner, d_outer)  # Default: 108.1
    vars["d_screws"] = str(d_screws)
    r_screws = d_screws / 2.0
    d_screw = random.uniform(0, d_outer-d_inner)  # Default: 4.0
    vars["d_screw"] = str(d_screw)
    r_screw = d_screw / 2.0
    d_screwcap = random.uniform(d_screw, (d_outer-d_inner) / 2.0)  # Default: 6.0
    vars["d_screwcap"] = str(d_screwcap)
    r_screwcap = d_screwcap / 2.0
    screw_sink = random.uniform(0, height)  # Default: 2.0
    vars["screw_sink"] = str(screw_sink)
    screw_count = random.randint(1, min(20,int(d_inner / d_screwcap)))  # Default: 4
    vars["screw_count"] = str(screw_count)

    fin_thickness = random.uniform(0, d_screwcap/2.0)  # Default: 2.5
    vars["fin_thickness"] = str(fin_thickness)
    fin_sink = random.uniform(0, height)  # Default: 1.5
    vars["fin_sink"] = str(fin_sink)
    fin_count = random.randint(0, min(40,int(d_screws/fin_thickness)))  # Default: 12
    vars["fin_count"] = str(fin_count)
    fin_angle = random.uniform(0, 360)  # Default: 30
    vars["fin_angle"] = str(fin_angle)
    fin_spacing = d_inner / (fin_count + 1)

    print("Vars:", vars)
    # Created using a loft
    cyl_outer = cq.Workplane("XY").circle(r_outer).workplane(offset=height).circle((r_outer + r_inner) / 2).loft()


    # Cut the inner cylinder from the outer cylinder
    frame = cyl_outer.faces(">Z").hole(diameter=d_inner,depth=100)

    # Add cbore holes for screws
    # There is a possibility of adding a selector here
    # TODO: Issue with cbore hole
    hole_workplane = frame.faces(">Z").workplane()
    for i in range(0, 360, 360 / screw_count):
        frame = hole_workplane.transformed(rotate=(0,0,i)).transformed(offset=(r_screws,0,0)).\
            cboreHole(cboreDepth=screw_sink, diameter=d_screw, cboreDiameter=d_screwcap)

    # Fins
    fins = cq.Workplane("XY")
    for fin_num in range(fin_count):
        fin = cq.Workplane("XY").box(fin_thickness, d_screws - d_screwcap, 100).rotate(axisStartPoint=(0,0,0), axisEndPoint=(0,1,0), angleDegrees=fin_angle).translate(vec=(-r_inner + fin_spacing + fin_spacing * fin_num, 0, 0))
        fins = fins.union(fin)
    # Common area of fins to be kept
    common_with = cq.Workplane("XY").circle(r_screws - r_screwcap).extrude(height - fin_sink)

    result = frame.union(fins.intersect(common_with))
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()