'''
Parametric Turners Cube
Link to similar Thingiverse file: https://www.thingiverse.com/thing:1072045
'''

import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()

L =  100
D1 = 90
D2 = 60
D3 = 30

T = L/8
X1 = L/2-T
X2 = L/2-2*T
X3 = L/2-3*T

# Make the cube
cube = cq.Workplane("XY").box(L,L,L)


# Add smallestholes

cube = cube.faces(">X").workplane().hole(D3, depth=X1)
cube = cube.faces("<X").workplane().hole(D3, depth=X1)
cube = cube.faces(">Y").workplane().hole(D3, depth=X1)
cube = cube.faces("<Y").workplane().hole(D3, depth=X1)
cube = cube.faces(">Z").workplane().hole(D3, depth=X1)
cube = cube.faces("<Z").workplane().hole(D3, depth=X1)

cube = cube.faces(">X").workplane().hole(D2, depth=X2)
cube = cube.faces("<X").workplane().hole(D2, depth=X2)
cube = cube.faces(">Y").workplane().hole(D2, depth=X2)
cube = cube.faces("<Y").workplane().hole(D2, depth=X2)
cube = cube.faces(">Z").workplane().hole(D2, depth=X2)
cube = cube.faces("<Z").workplane().hole(D2, depth=X2)

cube = cube.faces(">X").workplane().hole(D1, depth=X3)
cube = cube.faces("<X").workplane().hole(D1, depth=X3)
cube = cube.faces(">Y").workplane().hole(D1, depth=X3)
cube = cube.faces("<Y").workplane().hole(D1, depth=X3)
cube = cube.faces(">Z").workplane().hole(D1, depth=X3)
cube = cube.faces("<Z").workplane().hole(D1, depth=X3)



# Return the block
Part.show(cube.toFreecad())
tracker.add_change(shape_analyzer.get_line_num() - 1, block_xyz)
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, block)
listener.start_keyboard()
