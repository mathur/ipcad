//Turners Cube
$fn=360;

L = 100;
T = L/8;
D1 = 90;
D2 = 60;
D3 = 30;
X1 = L/2-T;
X2 = L/2-2*T;
X3 = L/2-3*T;

difference() {
	cube ([L,L,L], center = true);
	translate([0,0,X1]) cylinder (r = D1/2,h = T);
	translate([0,0,X2]) cylinder (r = D2/2,h = T);
	translate([0,0,X3]) cylinder (r = D3/2,h = T);
	translate([0,0,-X1-T]) cylinder (r = D1/2,h = T);
	translate([0,0,-X2-T]) cylinder (r = D2/2,h = T);
	translate([0,0,-X3-T]) cylinder (r = D3/2,h = T);
	translate([0,X1+2*T,0]) rotate ([90,0,0]) cylinder (r = D1/2,h = 2*T);
	translate([0,X2+T,0]) rotate ([90,0,0]) cylinder (r = D2/2,h = T);
	translate([0,X3+T,0]) rotate ([90,0,0]) cylinder (r = D3/2,h = T);
	translate([0,-X1,0]) rotate ([90,0,0]) cylinder (r = D1/2,h = 2*T);
	translate([0,-X2,0]) rotate ([90,0,0]) cylinder (r = D2/2,h = T);
	translate([0,-X3,0]) rotate ([90,0,0]) cylinder (r = D3/2,h = T);
	translate([X1,0,0]) rotate ([0,90,0]) cylinder (r = D1/2,h = 2*T);
	translate([X2,0,0]) rotate ([0,90,0]) cylinder (r = D2/2,h = T);
	translate([X3,0,0]) rotate ([0,90,0]) cylinder (r = D3/2,h = T);
	translate([-X1-2*T,0,0]) rotate ([0,90,0]) cylinder (r = D1/2,h = 2*T);
	translate([-X2-T,0,0]) rotate ([0,90,0]) cylinder (r = D2/2,h = T);
	translate([-X3-T,0,0]) rotate ([0,90,0]) cylinder (r = D3/2,h = T);

}
