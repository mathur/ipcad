$fn = 360;
// Diameter at bottom of plug
BottomDiameter = 22;

// Diameter at top of plug
TopDiameter = 28;

// Length of plug
PlugLength = 50;

// Handle width
HandleWidth=50;

cylinder(d1=BottomDiameter, d2=TopDiameter, h=PlugLength);
translate([0,0,PlugLength]) cube([10,HandleWidth,10],center=true);



