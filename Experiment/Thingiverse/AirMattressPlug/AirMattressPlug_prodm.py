# Inspired by the model at: https://www.thingiverse.com/thing:3818734
# Modified for random sampling of parameters

import cadquery as cq
import Part

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()

# min: 1, max: 100, default: 22
BottomDiameter = 22

# Diameter at top of plug
# min: 1, max: 100, default: 28
TopDiameter = 28
# Length of plug
# min: 1, max: 100, default: 50
PlugLength = 50
# Handle width
# min: 1, max: 100, default: 50
HandleWidth = 50

result = cq.Workplane("XY").circle(radius=BottomDiameter/2.0).workplane(offset=PlugLength).circle(TopDiameter/2.0).loft()
tracker.add_change(shape_analyzer.get_line_num() - 1, result)
result = result.faces(">Z").box(10,HandleWidth,10)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Return the block
Part.show(result.toFreecad())
tracker.add_change(shape_analyzer.get_line_num() - 1, result)
print("Starting event listener..")
listener = gui_observer.ViewListener(tracker, result)
listener.start_keyboard()
