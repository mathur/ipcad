# Inspired by the model at: https://www.thingiverse.com/thing:3818734
# Modified for random sampling of parameters

import cadquery as cq
import Part

import random
import json

# Number of iterations for the model
num_iter = 50

print("AirMattressPlug")

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    # Diameter at bottom of plug
    # min: 1, max: 100, default: 22
    BottomDiameter = random.uniform(1, 100)
    vars["BottomDiameter"] = str(BottomDiameter)
    # Diameter at top of plug
    # min: 1, max: 100, default: 28
    TopDiameter = random.uniform(1, 100)
    vars["TopDiameter"] = str(TopDiameter)
    # Length of plug
    # min: 1, max: 100, default: 50
    PlugLength = random.uniform(1, 100)
    vars["PlugLength"] = str(PlugLength)
    # Handle width
    # min: 1, max: 100, default: 50
    HandleWidth = random.uniform(1, 100)
    vars["HandleWidth"] = str(HandleWidth)

    result = cq.Workplane("XY").circle(radius=BottomDiameter/2.0).workplane(offset=PlugLength).circle(TopDiameter/2.0).loft()
    result = result.faces(">Z").box(10,HandleWidth,10)

    # Export to FreeCAD
    #Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL",tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()

