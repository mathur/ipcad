model_cq = "cq_1.stl";
model_openscad = "openscad_1.stl";

// Symmetric diff
union(){
    difference(){
        import(model_cq);
        import(model_openscad);
    }
    difference(){
        import(model_openscad);
        import(model_cq);
    }
}