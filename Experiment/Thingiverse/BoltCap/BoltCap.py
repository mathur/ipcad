# Inspired by https://www.thingiverse.com/thing:3814727

import cadquery as cq
import Part

import math
import json
import random

# [Hidden]

# Given the distance across flats of a hex, find the radius of a circle that scribes around the flats.
def hex_flats_to_radius(flats):
    return (flats/2.0)/math.cos(math.radians(180.0/6.0))

print("BoltCap")

# Number of iterations
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    # Distance across flats of nut in mm.
    nut_across_flats_mm = random.uniform(1, 100)  # min:1, default: 19.4
    vars["nut_across_flats_mm"] = str(nut_across_flats_mm)
    # Height of the nut in mm.
    nut_height_mm = random.uniform(1, 100)  # min:1, default: 13
    vars["nut_height_mm"] = str(nut_height_mm)
    # Length of the bolt shaft in mm (excludes bolt head).
    bolt_length_mm = random.uniform(1, 100)  # min:1, default: 33
    vars["bolt_length_mm"] = str(bolt_length_mm)
    # Diameter of the bolt shaft in mm
    bolt_diameter_mm = random.uniform(1, nut_across_flats_mm)  # min:1, default: 13.2
    vars["bolt_diameter_mm"] = str(bolt_diameter_mm)
    # Diameter of the air hole in mm. Use 0 if no hole is required.
    air_hole_diameter_mm = random.uniform(0, bolt_diameter_mm/2)  # min:0, default: 3
    vars["air_hole_diameter_mm"] = str(air_hole_diameter_mm)
    # The wall thickness in mm.
    wall_thickness_mm = random.uniform(1, 100)  # min:1, default: 1.2
    vars["wall_thickness_mm"] = str(wall_thickness_mm)
    # Additional clearance in mm for the bolt shaft.
    bolt_clearance_mm = random.uniform(0, wall_thickness_mm/2)  # min:0, default: 0.5
    vars["bolt_clearance_mm"] = str(bolt_clearance_mm)
    bolt_rad = bolt_diameter_mm/2.0
    nut_internal_rad=hex_flats_to_radius(nut_across_flats_mm)
    nut_external_rad=hex_flats_to_radius(nut_across_flats_mm + wall_thickness_mm)
    air_hole_rad = air_hole_diameter_mm/2

    # Outer shell
    cyl = cq.Workplane("XY").circle(nut_external_rad).extrude(bolt_length_mm - nut_external_rad/3.0)
    outer_shell = cyl.faces(">Z").sphere(nut_external_rad)

    # Inner cuts

    # Bolt cut - with slope on top
    cuts = cq.Workplane("XY").circle((bolt_diameter_mm + bolt_clearance_mm)/2.0).extrude(bolt_length_mm + bolt_clearance_mm)
    # Try to make a cone
    cuts = cuts.faces(">Z").circle(bolt_rad).workplane(offset=bolt_rad).circle(0.00001).loft()
    # Nut cut with slope on top

    cuts_hex = cq.Workplane("XY").polygon(nSides=6, diameter=2.0*nut_internal_rad).extrude(nut_height_mm + bolt_clearance_mm)
    cuts_hex = cuts_hex.faces(">Z").polygon(nSides=6, diameter=2.0*nut_internal_rad).workplane(offset=nut_height_mm/2.0 + bolt_clearance_mm).polygon(nSides=6, diameter=bolt_diameter_mm).loft()
    # Union the cuts
    cuts = cuts.union(cuts_hex)
    result = outer_shell.cut(cuts)
    # Air hole
    result = result.faces(">Z").hole(diameter=air_hole_diameter_mm, depth=bolt_length_mm + bolt_clearance_mm + nut_external_rad + (wall_thickness_mm*2))
    # Export to FreeCAD
    # Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
