/*
Andy Anderson 2019
*/

// preview[view:south, tilt:bottom diagonal]

// "Smoothness". Higher number (e.g. 100) is smoother.
$fn=360; // min/max:[25:100]

// Distance across flats of nut in mm.
nut_across_flats_mm = 19.4; // min:1

// Height of the nut in mm.
nut_height_mm = 13; // min:1

// Length of the bolt shaft in mm (excludes bolt head).
bolt_length_mm = 33; // min:1

// Diameter of the bolt shaft in mm
bolt_diameter_mm = 13.2; // min:1

// Diameter of the air hole in mm. Use 0 if no hole is required.
air_hole_diameter_mm = 3; // min:0

// The wall thickness in mm.
wall_thickness_mm = 1.2; // min:1

// Additional clearance in mm for the bolt shaft.
bolt_clearance_mm= 0.5; // min:0

/* [Hidden] */

// Given the distance across flats of a hex, find the radius of a circle that scribes around the flats.
function hex_flats_to_radius(flats) = (flats/2)/cos(180/6);

bolt_rad = bolt_diameter_mm/2;
nut_internal_rad=hex_flats_to_radius(nut_across_flats_mm);
nut_external_rad=hex_flats_to_radius(nut_across_flats_mm + wall_thickness_mm);
air_hole_rad = air_hole_diameter_mm/2;


difference()
{
    // Outer shell
    union()
    {
        cylinder(h=bolt_length_mm - (nut_external_rad/3), r=nut_external_rad, center=false);
        translate([0, 0, bolt_length_mm-(nut_external_rad/3)])
            sphere(r=nut_external_rad);
    }
    
    // Inner cuts
    union()
    {
        // Bolt cut - with slope on top
        cylinder(h=bolt_length_mm + bolt_clearance_mm, d=bolt_diameter_mm + bolt_clearance_mm);
        translate([0, 0, bolt_length_mm + bolt_clearance_mm])
            cylinder(h=bolt_rad, r1=bolt_rad, r2=0);
        
        // Nut cut - with slope on top
        cylinder($fn=6, h=nut_height_mm + bolt_clearance_mm, r=nut_internal_rad);
        translate([0, 0, nut_height_mm + bolt_clearance_mm])
        {
            cylinder($fn=6, h=nut_height_mm/2 + bolt_clearance_mm, r1=nut_internal_rad, r2=bolt_rad);
        }
        
        // Air hole
        cylinder(h=bolt_length_mm + bolt_clearance_mm + nut_external_rad + (wall_thickness_mm*2), r=air_hole_rad);
    }
}