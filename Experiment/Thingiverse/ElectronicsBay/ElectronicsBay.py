# Inspired by design on: https://www.thingiverse.com/thing:3859829
# Comment line 2306 of cq.py
import cadquery as cq
import json
import random

print("Electronics Bay")

# Number of iterations
num_iter = 50

# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()

# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    # configuration
    # Enter desired diameter of plates
    # d1=54.7
    d1 = random.uniform(1, 100)  # Default:40
    vars["d1"] = str(d1)

    # enter top bay height
    tb = random.uniform(1, 100)  # Default:50
    vars["tb"] = str(tb)

    # enter desired diameter of spacers
    sp = random.uniform(0, d1/2.0)  # Default:4
    vars["sp"] = str(sp)

    #  shelf spacing
    ss = random.uniform(0, tb/4.0)  # Default:10
    vars["ss"] = str(ss)

    # enter shelf thickness
    st = random.uniform(0, tb/4.0)  # Default:2
    vars["st"] = str(st)

    ra1 = d1/2.0
    ra2 = ((ra1) * -1.0)
    sp1 = sp/2.0
    th = (ss*2.0)+(st*4.0) + tb

    print("Iteration", iter)
    print(vars)
    # Base plate
    base = cq.Workplane("XY").circle(ra1).extrude(st)

    # Layer 2 plate
    base = base.faces(">Z").workplane(offset=ss).circle(ra1).extrude(st)
    # layer_3
    base = base.faces(">Z").workplane(offset=ss).circle(ra1).extrude(st)

    # Top plate
    top = cq.Workplane("XY").workplane(offset=th-st).circle(ra1).extrude(st)

    base = base.union(top)

    # Holders
    holders_a = cq.Workplane("XY").transformed(offset=((ra1-(sp/2)),0,0)).circle(sp1).extrude(th, combine=False)
    holders_b = cq.Workplane("XY").transformed(offset=((ra2+(sp/2)),0,0)).circle(sp1).extrude(th, combine=False)
    holders_c = cq.Workplane("XY").transformed(offset=(0,(ra1-(sp/2.0)),0)).circle(sp1).extrude(th, combine=False)
    holders_d = cq.Workplane("XY").transformed(offset=(0,(ra2+(sp/2)),0)).circle(sp1).extrude(th, combine=False)

    holders = holders_a.union(holders_b.union(holders_c.union(holders_d)))

    # Combine the holders with the base
    result = base.union(holders)
    # Export to FreeCAD
    # Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars
base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
