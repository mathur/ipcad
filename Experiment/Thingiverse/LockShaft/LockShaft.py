# Inspired by the design at: https://www.thingiverse.com/thing:3841621
import cadquery as cq
import random
import json
import math

print("LockShaft")

# Number of iterations
num_iter = 50
# Prepare the json file
json_fname = "Vars.json"
json_file = open(json_fname, "w+")
base_dict = dict()
param_sets = dict()
# Number of experiment iterations
for iter in range(num_iter):
    vars = dict()
    # [Dimensions of triangular drive:]
    # (across points measurment)
    Key_AP = random.uniform(1, 100)  # Default: 9.2
    vars["Key_AP"] = str(Key_AP)
    # (length)
    Key_l = random.uniform(1, 100)  # Default: 8.5
    vars["Key_l"] = str(Key_l)
    # (tip radius)
    Key_tr = random.uniform(0, 1)  # Default: 0.5
    vars["Key_tr"] = str(Key_tr)

    # [Flange dimensions]
    # (diameter)
    Flange_d = random.uniform(1, 100)  # Default: 15.8
    vars["Flange_d"] = str(Flange_d)
    # (length)
    Flange_l = random.uniform(1, 100)  # Default: 1.9
    vars["Flange_l"] = str(Flange_l)

    # [Shaft dimensions]
    # (diameter)
    Shaft_d = random.uniform(1, 100)  # Default: 9.8
    vars["Shaft_d"] = str(Shaft_d)
    # (length)
    Shaft_l = random.uniform(1, 100)  # Default: 5.7
    vars["Shaft_l"] = str(Shaft_l)

    # [Locking pawl drive boss dimensions]
    # (width)
    Drive_w = random.uniform(1, 100)  # Default: 7.1
    vars["Drive_w"] = str(Drive_w)
    # (breadth)
    Drive_b = random.uniform(1, 100)  # Default: 6.1
    vars["Drive_b"] = str(Drive_b)
    # (length)
    Drive_l = random.uniform(1, 100)  # Default: 9.1
    vars["Drive_l"] = str(Drive_l)
    # (angle offset)
    Drive_a = random.uniform(0, 360) # Default: 0
    vars["Drive_a"] = str(Drive_a)

    # Not to be changed (advice from OpenSCAD)
    Key_r = (0.57735 * Key_AP) - Key_tr
    Total_l = Key_l + Flange_l + Shaft_l + Drive_l
    # (tip radius)
    Key_tr = random.uniform(0, Key_r/2.0)  # Default: 0.5
    vars["Key_tr"] = str(Key_tr)

    base = cq.Workplane("XY").polygon(nSides=3, diameter=(4.0 / 1.73205) * (Key_r + Key_tr)).extrude(Key_l)
    base = base.edges("|Z").fillet(Key_tr)

    flange = base.faces(">Z").circle(Flange_d/2.0).extrude(Flange_l)
    shaft = flange.faces(">Z").workplane(offset=-0.01).circle(Shaft_d/2.0).extrude(Shaft_l + 0.01)

    drive = shaft.faces(">Z").workplane().transformed(offset=(0,0,(Drive_l/2.0)-0.01), rotate=(0,0,Drive_a))\
        .box(Drive_b, Drive_w, Drive_l + 0.02)

    cut_a = cq.Workplane("XY").workplane(offset=0.5).circle(2.8/2.0).extrude(0.6*Total_l)
    cut = cut_a.faces(">Z").workplane(offset=0.49).circle(3.25/2.0).extrude(0.4*Total_l)

    result = drive.cut(cut)
    # Export to FreeCAD
    # Part.show(result.toFreecad())
    # Export STL
    f_name = "cq_" + str(iter) + ".stl"
    f = open(f_name, 'w+')
    cq.exporters.exportShape(shape=result, fileLike=f, exportType="STL", tolerance=0.002777)
    f.close()
    # Add this parameter set to dictionary
    param_sets[str(iter)] = vars

base_dict["parameterSets"] = param_sets
base_dict["fileFormatVersion"] = "1"
# Write JSON file
json_file.write(json.dumps(base_dict))
json_file.close()
