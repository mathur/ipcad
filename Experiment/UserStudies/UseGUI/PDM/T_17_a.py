
from prodm import gui_observer
from prodm import shape_analyzer

import cadquery as cq

tracker = shape_analyzer.Tracker()

exploded = False  # when true, moves the base away from the top so we see
showTop = True  # When true, the top is rendered.
showCover = True  # When true, the cover is rendered

width = 2.2  # Nominal x dimension of the part
height = 0.5  # Height from bottom top to the top of the top :P
length = 1.5  # Nominal y dimension of the part
trapezoidFudge = 0.7  # ratio of trapezoid bases. set to 1.0 for cube
xHoleOffset = 0.500  # Holes are distributed symmetrically about each axis
yHoleOffset = 0.500
zFilletRadius = 0.50  # Fillet radius of corners perp. to Z axis.
yFilletRadius = 0.250  # Fillet radius of the top edge of the case
lipHeight = 0.1  # The height of the lip on the inside of the cover
wallThickness = 0.06  # Wall thickness for the case
coverThickness = 0.2  # Thickness of the cover plate
holeRadius = 0.30  # Button hole radius
counterSyncAngle = 100  # Countersink angle.

xyplane = cq.Workplane("XY")
yzplane = cq.Workplane("YZ")


def trapezoid(b1, b2, h):
    "Defines a symmetrical trapezoid in the XY plane."

    y = h / 2
    x1 = b1 / 2
    x2 = b2 / 2
    return (xyplane.moveTo(-x1, y)
            .polyline([(x1, y),
                       (x2, -y),
                       (-x2, -y)]).close())


# Defines our base shape: a box with fillets around the vertical edges.
# This has to be a function because we need to create multiple copies of
# the shape.
def base(h):
    tmp = trapezoid(width, width * trapezoidFudge, length).extrude(h).translate((0, 0, height / 2))
    tracker.add_change(shape_analyzer.get_line_num() - 1, tmp)
    return tmp


top = base(height)

listener = gui_observer.ViewListener(tracker, top, show_obj=True)
listener.start_keyboard()
