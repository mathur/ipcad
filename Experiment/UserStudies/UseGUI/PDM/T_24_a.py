import Part
import cadquery
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# Set up the length, width, and thickness
base = cadquery.Workplane("XY")

# Draw half the profile of the bottle and extrude it
cyl1 = base.circle(20).extrude(50)
tracker.add_change(shape_analyzer.get_line_num() - 1, cyl1)

listener = gui_observer.ViewListener(tracker, cyl1, show_obj=True)
listener.start_keyboard()