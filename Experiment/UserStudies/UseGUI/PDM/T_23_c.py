# 3d printer for mounting hotend to X-carriage inspired by the P3steel Toolson
# edition - http://www.thingiverse.com/thing:1054909
import cadquery as cq

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()

def move_to_center(cqObject, shape):
    '''
    Moves the origin of the current Workplane to the center of a given
    geometry object
    '''

    # transform to workplane local coordinates
    shape_center = shape.Center().sub(cqObject.plane.origin)

    # project onto plane using dot product
    x_offset = shape_center.dot(cqObject.plane.xDir)
    y_offset = shape_center.dot(cqObject.plane.yDir)

    return cqObject.center(x_offset, y_offset)

# Parameter definitions

main_plate_size_y = 67  # size of the main plate in y direction
main_plate_size_x = 50.  # size of the main plate in x direction
main_plate_thickness = 10.  # thickness of the main plate

wing_size_x = 10.  # size of the side wing supporting the bridge in x direction
wing_size_y = 10.  # size of the side wing supporting the bridge in y direction

bridge_depth = 35.  # depth of the bridge

support_depth = 18.  # depth of the bridge support

cutout_depth = 15.  # depth of the hotend cutout
cutout_rad = 8.     # radius of the cutout (cf groove mount sizes of E3D hotends)
cutout_offset = 2.  # delta radius of the second cutout (cf groove mount sizes of E3D hotends)

extruder_hole_spacing = 50.  # spacing of the extruder mounting holes (Wade's geared extruder)

m4_predrill = 3.7  # hole diameter for m4 tapping
m3_predrill = 2.5  # hole diameter for m3 tapping
m3_cbore = 5.      # counterbore size for m3 socket screw

mounting_hole_spacing = 28.  # spacing of the mounting holes for attaching to x-carriage

aux_hole_depth = 6.    # depth of the auxiliary holes at the sides of the object
aux_hole_spacing = 5.  # spacing of the auxiliary holes within a group
aux_hole_N = 2         # number of the auxiliary hole per group

# make the main plate
res = cq.Workplane('front').box(main_plate_size_x,
                                main_plate_size_y,
                                main_plate_thickness)
tracker.add_change(shape_analyzer.get_line_num() - 1, res)

def add_wing(obj, sign=1):
    '''
    Adds a wing to the main plate, defined to keep the code DRY
    '''
    obj = obj.workplane()\
             .hLine(sign*wing_size_x)\
             .vLine(-wing_size_y)\
             .line(-sign*wing_size_x, -2*wing_size_y)\
             .close().extrude(main_plate_thickness)
    return obj

# add wings

# add right wing
res = res.faces('<Z').vertices('>XY')
res = add_wing(res)
tracker.add_change(shape_analyzer.get_line_num() - 1, res)
# store sides of the plate for further reuse, their area is used later on to calculate "optimum" spacing of the aux hole groups
face_right = res.faces('>X[1]').val()
face_left = res.faces('>X[-2]').val()

# add left wing
res = res.faces('<Z').vertices('>Y').vertices('<X')
tracker.add_change(shape_analyzer.get_line_num() - 1, res)
res = add_wing(res, -1)
tracker.add_change(shape_analyzer.get_line_num() - 1, res)

# show the result

listener = gui_observer.ViewListener(tracker, res, show_obj=True)
listener.start_keyboard()