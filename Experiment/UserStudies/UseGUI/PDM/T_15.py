# This example is meant to be used from within the CadQuery module of FreeCAD.
import cadquery
import FreeCAD
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# Create a new document that we can draw our model on
newDoc = FreeCAD.newDocument()

# Shows a 1x1x1 FreeCAD cube in the display
initialBox = newDoc.addObject("Part::Box", "initialBox")
newDoc.recompute()

# Make a CQ object
cqBox = cadquery.CQ(cadquery.Solid(initialBox.Shape))
tracker.add_change(shape_analyzer.get_line_num() - 1, cqBox)

listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()
