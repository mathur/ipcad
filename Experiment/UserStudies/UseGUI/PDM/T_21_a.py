# Example using advanced logical operators in string selectors to select only
# the inside edges on a shelled cube to chamfer.
import cadquery as cq

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
result = cq.Workplane("XY").box(2, 2, 2)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)


listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()

