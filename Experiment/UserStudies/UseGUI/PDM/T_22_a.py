import cadquery as cq
from cadquery import selectors

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
model = cq.Workplane("XY").box(15.0, 15.0, 2.0)
tracker.add_change(shape_analyzer.get_line_num() - 1, model)


listener = gui_observer.ViewListener(tracker, model, show_obj=True)
listener.start_keyboard()