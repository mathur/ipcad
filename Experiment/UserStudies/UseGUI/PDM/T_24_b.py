import Part
import cadquery
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# Set up the length, width, and thickness
base = cadquery.Workplane("XY")

# Draw half the profile of the bottle and extrude it
cyl1 = base.circle(20).extrude(50)
tracker.add_change(shape_analyzer.get_line_num() - 1, cyl1)

cyl2 = cyl1.faces(">Z").workplane().circle(15).extrude(15)
tracker.add_change(shape_analyzer.get_line_num() - 1, cyl2)

result = cyl1.union(cyl2)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()