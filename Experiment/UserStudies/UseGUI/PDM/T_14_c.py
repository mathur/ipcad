# Some code for Filament Storage Dowel End Cap: https://www.thingiverse.com/thing:3324110

import cadquery as cq
import Part
from prodm import gui_observer
from prodm import shape_analyzer

# Program variables
base_l = 200.0
base_w = 200.0
base_h = 10.0
base_r = 40.0
tube_r = 25.0
tube_h = 125.0
tube_shell_r = -2.0
fillet_r = 6.0

#Create tracker object
tracker = shape_analyzer.Tracker()

# Make the base
base = cq.Workplane("XY").box(base_l, base_w, base_h)
tracker.add_change(shape_analyzer.get_line_num() - 1, base)
# Make the tube
tube = base.faces(">Z").circle(tube_r).extrude(tube_h, combine=False)
tracker.add_change(shape_analyzer.get_line_num() - 1, tube)

tube = tube.faces(">Z").shell(tube_shell_r)
tracker.add_change(shape_analyzer.get_line_num() - 1, tube)

result = base.union(tube)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()