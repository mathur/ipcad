import cadquery as cq

from prodm import gui_observer
from prodm import shape_analyzer

# These can be modified rather than hardcoding values for each dimension.
length = 80.0                   # Length of the block
height = 60.0                   # Height of the block
thickness = 10.0                # Thickness of the block
center_hole_dia = 22.0          # Diameter of center hole in block
cbore_hole_diameter = 2.4       # Bolt shank/threads clearance hole diameter
cbore_diameter = 4.4            # Bolt head pocket hole diameter
cbore_depth = 2.1               # Bolt head pocket hole depth

tracker = shape_analyzer.Tracker()
# Create a 3D block based on the dimensions above and add a 22mm center hold
# and 4 counterbored holes for bolts
# Create a box
result = cq.Workplane("XY").box(length, height, thickness)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

result = result.faces(">Z").workplane().hole(center_hole_dia)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Displays the result of this script

# Start the GUI listener
listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()