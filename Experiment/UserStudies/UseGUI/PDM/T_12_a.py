import cadquery as cq

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# Create a box
c = cq.Workplane("XY").box(1, 1, 1)
tracker.add_change(shape_analyzer.get_line_num() - 1, c)

# Displays the result of this script

listener = gui_observer.ViewListener(tracker, c, show_obj=True)
listener.start_keyboard()