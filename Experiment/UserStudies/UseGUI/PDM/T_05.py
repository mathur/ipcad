import cadquery as cq

from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# Create a 3D box
result = cq.Workplane("XY").box(3, 2, 0.5)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

# Displays the result of this script

listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()