# ---------------------------------
# Trial 12
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Create a box
c = cq.Workplane("XY").box(1, 1, 1)

# See Trial 12 (a) selection
c = c.faces("TODO").workplane() \
     .circle(0.25).cutThruAll()

# See Trial 12 (b) selection
result = c.faces("TODO").workplane(-0.5).split(keepTop=True)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex021

