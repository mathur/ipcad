# ---------------------------------
# Trial 22
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
from cadquery import selectors
import Part

model = cq.Workplane("XY").box(15.0, 15.0, 2.0)

# See Trial 22 (a) selection
model = model.faces("TODO").rect(10.0, 10.0, forConstruction=True)\
		     .vertices().cskHole(2.0, 4.0, 82)

# See Trial 22 (b) selection
model = model.faces("TODO").circle(4.0).extrude(10.0)

# See Trial 22 (c) selection
model = model.faces("TODO").hole(6)

result = model.faces('<Z[1]').edges(selectors.NearestToPointSelector((0.0, 0.0))).fillet(1)

Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex035