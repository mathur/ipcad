# ---------------------------------
# Trial 07
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Create a box
result = cq.Workplane("XY").box(2, 2, 0.5)

# See Trial 07 (a) selection
result = result.faces("TODO").workplane() \
               .rect(1.5, 1.5, forConstruction=True).vertices() \
               .hole(0.125)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex016