# ---------------------------------
# Trial 09
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Create a box of appropriate dimensions
result = cq.Workplane("XY").box(4.0, 4.0, 0.25)

# See Trial 09 (a) selection
result = result.faces("TODO") \
               .circle(1.5).workplane(offset=3.0) \
               .rect(0.75, 0.5) \
               .loft(combine=True)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex018