# ---------------------------------
# Trial 13
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Set up the length, width, and thickness
(L, w, t) = (20.0, 6.0, 3.0)
s = cq.Workplane("XY")

# Draw half the profile of the bottle and extrude it
p = s.center(-L / 2.0, 0).vLine(w / 2.0) \
     .threePointArc((L / 2.0, w / 2.0 + t), (L, w / 2.0)).vLine(-w / 2.0) \
     .mirrorX().extrude(30.0, True)

# See Trial 13 (a) selection
p.faces("TODO").workplane().circle(3.0).extrude(2.0, True)

# See Trial 13 (b) selection
result = p.faces("TODO").shell(0.3)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex022