# ---------------------------------
# Trial 01
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------

import cadquery as cq
import Part

# These can be modified rather than hardcoding values for each dimension.
length = 80.0               # Length of the block
height = 60.0               # Height of the block
thickness = 10.0            # Thickness of the block
center_hole_dia = 22.0      # Diameter of center hole in block

# Create a block based on the dimensions above and add a 22mm center hole.
result = cq.Workplane("XY").box(length, height, thickness)
# See Trial 01 (a) selection
result = result.faces("TODO").workplane().hole(center_hole_dia)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex002