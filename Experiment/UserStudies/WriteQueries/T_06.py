# ---------------------------------
# Trial 06
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Create a box
result = cq.Workplane("front").box(4.0, 4.0, 0.25)

# See Trial 06 (a) selection
result = result.faces("TODO") \
               .workplane() \
               .transformed(offset=(0, -1.5, 1.0), rotate=(60, 0, 0)) \
               .rect(1.5, 1.5, forConstruction=True).vertices().hole(0.25)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex015