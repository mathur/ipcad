# ---------------------------------
# Trial 24
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import Part
import cadquery

# Set up the length, width, and thickness
base = cadquery.Workplane("XY")

# Draw half the profile of the bottle and extrude it
cyl1 = base.circle(20).extrude(50)

# See Trial 24 (a) selection
cyl2 = cyl1.faces("TODO").workplane().circle(15).extrude(15)
result = cyl1.union(cyl2)

# See Trial 24 (b) selection
result = result.edges("TODO").fillet(1)

# See Trial 24 (c) selection
result = result.faces("TODO").shell(-0.5)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to CylindricalBottle