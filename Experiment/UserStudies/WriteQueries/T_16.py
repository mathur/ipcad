# ---------------------------------
# Trial 16
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
# This script can create any regular rectangular Lego(TM) Brick
import cadquery as cq
import Part

#####
# Inputs
######
lbumps = 1       # number of bumps long
wbumps = 1        # number of bumps wide
thin = True  # True for thin, False for thick

#
# Lego Brick Constants-- these make a lego brick a lego :)
#
pitch = 8.0
clearance = 0.1
bumpDiam = 4.8
bumpHeight = 1.8
if thin:
    height = 3.2
else:
    height = 9.6

t = (pitch - (2 * clearance) - bumpDiam) / 2.0
postDiam = pitch - t  # works out to 6.5
total_length = lbumps*pitch - 2.0*clearance
total_width = wbumps*pitch - 2.0*clearance

# make the base
s = cq.Workplane("XY").box(total_length, total_width, height)

# See Trial 16 (a) selection
s = s.faces("TODO").shell(-1.0 * t)

# See Trial 16 (b) selection
s = s.faces("TODO").workplane(). \
    rarray(pitch, pitch, lbumps, wbumps, True).circle(bumpDiam / 2.0) \
    .extrude(bumpHeight)

# See Trial 16 (c) selection
tmp = s.faces("TODO").workplane(invert=True)

if lbumps > 1 and wbumps > 1:
    tmp = tmp.rarray(pitch, pitch, lbumps - 1, wbumps - 1, center=True). \
        circle(postDiam / 2.0).circle(bumpDiam / 2.0).extrude(height - t)
elif lbumps > 1:
    tmp = tmp.rarray(pitch, pitch, lbumps - 1, 1, center=True). \
        circle(t).extrude(height - t)
elif wbumps > 1:
    tmp = tmp.rarray(pitch, pitch, 1, wbumps - 1, center=True). \
        circle(t).extrude(height - t)
else:
    tmp = s

# Render the solid
Part.show(tmp.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex026
