# ---------------------------------
# Trial 02
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------

import cadquery as cq
import Part

# These can be modified rather than hardcoding values for each dimension.
length = 80.0                   # Length of the block
height = 60.0                   # Height of the block
thickness = 10.0                # Thickness of the block
center_hole_dia = 22.0          # Diameter of center hole in block
cbore_hole_diameter = 2.4       # Bolt shank/threads clearance hole diameter
cbore_diameter = 4.4            # Bolt head pocket hole diameter
cbore_depth = 2.1               # Bolt head pocket hole depth

# Create a 3D block based on the dimensions above and add a 22mm center hold
# and 4 counterbored holes for bolts
# Create a box
result = cq.Workplane("XY").box(length, height, thickness)
# See Trial 02 (a) selection
result = result.faces("TODO").workplane().hole(center_hole_dia)
# See Trial 02 (b) selection
result = result.faces("TODO").workplane() \
			   .rect(length - 8.0, height - 8.0, forConstruction=True) \
			   .vertices().cboreHole(cbore_hole_diameter, cbore_diameter, cbore_depth)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex003