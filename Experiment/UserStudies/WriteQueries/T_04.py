# ---------------------------------
# Trial 04
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part 

# Create a box
result = cq.Workplane("XY").box(3, 2, 0.5)

# See Trial 04 (a) selection
result = result.vertices("TODO").workplane()

# A circle is drawn with the selected vertex as its center.
# The circle is cut down through the box to cut the corner out.
result = result.circle(1.0).cutThruAll()

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex013
