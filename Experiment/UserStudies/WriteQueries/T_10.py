# ---------------------------------
# Trial 10
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import cadquery as cq
import Part

# Create a box
result = cq.Workplane("XY").box(4, 2, 0.5)

# See Trial 10 (a) selection
result = result.faces("TODO").workplane().rect(3.5, 1.5, forConstruction=True) \
               .vertices().cskHole(0.125, 0.25, 82.0, depth=None)

# Displays the result of this script
Part.show(result.toFreecad())

# ---------------------------------
# For experimenter use only: this example corresponds to Ex019