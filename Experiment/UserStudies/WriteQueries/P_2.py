# ---------------------------------
# Practice 2
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import Part
import cadquery

# We make a rectangular bottle here
# Make a box of relevant length, width and height
base = cadquery.Workplane("XY").box(15, 15, 30)

# See Practice 2 (a) selection
top = base.faces("TODO").workplane().box(10, 10, 20)
# Union the base and the top
result = base.union(top)

# See Practice 2 (b) selection
result = result.faces("TODO").shell(-1.0)

# Displays the result of this script
Part.show(result.toFreecad())