Examples in `Run` work automatically with a single click on a Windows 10 machine.
Before running, you need to mass Find & Replace:
1. "C:\Program Files\FreeCAD 0.17\bin" -> the location of your FreeCAD executable
2. "C:\Work\CADProjects\cq\ProDMCAD" -> the location of this project on your system
3. "C:\Work\CADProjects\cq" -> the location of cadquery on your system
