# ---------------------------------
# Click on Practice 1
# Fill up the query corresponding  to selection(s) in the screenshot(s)
# ---------------------------------
import Part
import cadquery

"""
We make an egg holder: essentially a cubical base with a spherical cut.
The edges of the spherical cut are filleted (rounded) so that 
"""
# Make a cubical base of given dimensions
base = cadquery.Workplane("XY").box(27, 27, 27)

# See Practice 1 (a) selection
sph = base.faces("TODO").sphere(15, combine=False)
# Cut the sphere from the base
result = base.cut(sph)

# See Practice 1 (a) selection
result = result.edges("TODO").fillet(1)

# Display the result of this script
Part.show(result.toFreecad())