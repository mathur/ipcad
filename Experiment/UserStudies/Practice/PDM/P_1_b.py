import cadquery
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
"""
We make an egg holder: which is essentially a rectangular base with a spherical cut.
The edge of the spherical cut is 
"""
base = cadquery.Workplane("XY").box(27, 27, 27)
tracker.add_change(shape_analyzer.get_line_num() - 1, base)

sph = base.faces(">Z").sphere(15, combine=False)
tracker.add_change(shape_analyzer.get_line_num() - 1, sph)

result = base.cut(sph)
tracker.add_change(shape_analyzer.get_line_num() - 1, result)

listener = gui_observer.ViewListener(tracker, result, show_obj=True)
listener.start_keyboard()