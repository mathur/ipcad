import cadquery
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
"""
We make an egg holder: which is essentially a rectangular base with a spherical cut.
The edge of the spherical cut is 
"""
base = cadquery.Workplane("XY").box(27, 27, 27)
tracker.add_change(shape_analyzer.get_line_num() - 1, base)

listener = gui_observer.ViewListener(tracker, base, show_obj=True)
listener.start_keyboard()