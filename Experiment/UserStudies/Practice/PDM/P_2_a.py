import cadquery
from prodm import gui_observer
from prodm import shape_analyzer

tracker = shape_analyzer.Tracker()
# We make a rectangular bottle here
# Make a box of relevant length, width and height
base = cadquery.Workplane("XY").box(15, 15, 30)
tracker.add_change(shape_analyzer.get_line_num() - 1, base)

listener = gui_observer.ViewListener(tracker, base, show_obj=True)
listener.start_keyboard()