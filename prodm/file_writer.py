"""
This script provides helper functions to add text or modify the original script
"""
loc_script = "C:\\Work\\CADProjects\\cq\\ProDMCAD\\prodm\\Example-synth.py"


def write_edge_selector(selector, operation):
    line_to_add = "result = result.edges(\""
    line_to_add = line_to_add + selector + "\")"
    line_to_add = line_to_add + "." + operation
    f = open(loc_script, "w+")
    f.write(line_to_add + "\n")
    f.close()
